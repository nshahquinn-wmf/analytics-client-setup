#!/usr/bin/env bash
script_dir="$( dirname -- "$( readlink -f -- "$0"; )"; )"

# Symlink dotfiles (ln complains but doesn't error if the link already exists)
# This is first so the Conda commands use these settings
jupyterlab_settings_dir=$CONDA_PREFIX/etc/jupyter/lab/user-settings/
mkdir -p $jupyterlab_settings_dir
ln -s $script_dir/config/jupyterlab $jupyterlab_settings_dir/@jupyterlab

ln -s $script_dir/config/.gitconfig ~/.gitconfig
ln -s $script_dir/config/.gitignore_global ~/.gitignore_global

ln -s $script_dir/config/condarc ~/.condarc

# Do ALL THE UPDATES.
conda env update -f $script_dir/environment_updates.yml

# Install the R kernel spec
R --no-echo -e "IRkernel::installspec()"

# Quarto adds a shell script to the environment's initialization routine.
# Run it now so Quarto is available without waiting for the next environment restart.
. $CONDA_PREFIX/etc/conda/activate.d/quarto.sh

# Install key packages in editable mode
if [ ! -d ~/wmfdata-python ]
then
    git clone https://gitlab.wikimedia.org/repos/data-engineering/wmfdata-python.git
fi
pip install -e ~/wmfdata-python

if [ ! -d ~/nshahquinn ]
then
    git clone https://github.com/nshahquinn/personal-python.git nshahquinn
fi
pip install -e ~/nshahquinn

# Clone perennial repo
if [ ! -d ~/miscellaneous-notebooks ]
then
    git clone https://gitlab.wikimedia.org/nshahquinn-wmf/miscellaneous-notebooks.git
fi

# Set up nbdime as the Git diff and merge driver for notebooks
nbdime config-git --enable --global

echo "Everything has been set up!"
echo "NOTE: The Quarto JupyterLab extension will not work until the Jupyter server is restarted."
