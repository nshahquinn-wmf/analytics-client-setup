This tool sets up a Conda environment on one of the analytics clients just how I like it.

To use, simply run `setup.sh`.
